/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/
document.addEventListener('DOMContentLoaded', function(){
  const OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  let currentPosition = 0;
  let prevPosition = 0;
  let wereShowed = [currentPosition];

  const slider = document.getElementById('slider');
  const prev = document.getElementById('PrevSlide');
  const next = document.getElementById('NextSlide');
  
  function createImg (source) {
    imgSlide = new Image(512, 512);
    imgSlide.src = source; 
    imgSlide.classList.add('js-slide', 'slide', '-active');
    imgSlide.setAttribute('data-id', currentPosition + 1);
    slider.appendChild(imgSlide);
  }

  function magicSlide (direction) {
    let prevSlide = slider.querySelector(`[data-id='${prevPosition + 1}']`);
    let currentSlide = slider.querySelector(`[data-id='${currentPosition + 1}']`);
    prevSlide.classList.remove('-active', '-show-to-left', '-show-to-right');
    currentSlide.classList.add(`-show-to-${direction}`);
  }

  function changeSlide (path) {
    prevPosition = currentPosition;
    currentPosition = currentPosition + path;
    path == 1 ? direction = 'right' : direction = 'left';
    const length = OurSliderImages.length; 
    const lastElement = length - 1;
    if (currentPosition > lastElement) {
      currentPosition = 0;
    }
    if (currentPosition < 0) {
      currentPosition = lastElement;
    }
    if (!wereShowed.includes(currentPosition)) {
      wereShowed.push(currentPosition);
      createImg(OurSliderImages[currentPosition]);
    }
    magicSlide(direction, prevPosition);
  }

  window.addEventListener('load', function(){
    createImg(OurSliderImages[currentPosition]);
  });

  prev.addEventListener('click', function(){;
    changeSlide(-1);
  });
  next.addEventListener('click', function(){
    changeSlide(1);
  });
});